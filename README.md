## Mina Provisioner
This is a set of ansible scripts to provision a Mina mining server in a secure way.
It will do the following
* Create one machine secure it
* Create two users *admin* and *mina*. You provide a public SSH-key that will be authorized for the admin user. *admin* will be able to do passwordless sudo and *mina* will be able to run docker. root will be disabled.
* Install *docker*, *docker-compose* and *ntp*
* Provide a [docker stack](https://gitlab.com/tedsteen/mina-miner/-/blob/master/roles/miner/files/docker-compose.yaml) with the [miner](https://github.com/MinaProtocol/mina), [this dashboard](https://github.com/dsrvlabs/mina-performance-dashboard) and [this snark-stopper](https://github.com/c29r3/mina-snark-stopper).

```bash
# Install deps
pip install ansible hcloud
ansible-galaxy collection install hetzner.hcloud community.general
ansible-galaxy install dev-sec.os-hardening dev-sec.ssh-hardening jnv.unattended-upgrades geerlingguy.ntp geerlingguy.docker

# Use vagrant environment (or dev or prod)
./env.sh vagrant

# Create vagrant machine (or hetzner.yaml) and do base provisioning
ansible-playbook vagrant.yaml base.yaml

# Install the Mina docker-compose stack
ansible-playbook install-mina.yaml

# SSH to the host with admin and su to mina
CODA_PRIVKEY_PASS="<your-password>" docker-compose -f miner/docker-compose.yaml up -d --build --remove-orphans
```

### Mina container trix
First ssh to the machine
```bash
# For details see https://docs.minaprotocol.com/en/using-mina/cli-reference or
docker exec mina mina client help

# get client status
docker exec mina mina client status
```

### Generate mina keys:
```bash
cd ~
docker run -it --rm --volume $(pwd)/keys:/keys minaprotocol/generate-keypair:0.2.12-718eba4 -privkey-path /keys/my-wallet
# keys are now in the directory keys/

# ensure the permissions are set properly for the private key file
chmod 700 keys/my-wallet.pub
chmod 600 keys/my-wallet

# Validate the private key
chmod 700 keys
docker run -it --rm --entrypoint=mina-validate-keypair --volume $(pwd)/keys:/keys minaprotocol/generate-keypair:0.2.12-718eba4 -privkey-path /keys/my-wallet
```

### Setup graphs in grafana
1. Go to grafana on `http://<server-ip>:3000` (admin/admin)
2. Add prometheus datasource host `http://prometheus:9090`
3. Import dashboard https://grafana.com/dashboards/12840

### TODO
- ansible vault for securing secrets
- https://towerstake.com/mina-uptime-leaderboard/
- hardware?
    - https://www.netcup.eu/bestellen/produkt.php?produkt=2604 <--- seems nice like most value for money.
    - https://contabo.com/en/vps/vps-xl-ssd/